# creates: mpf.png

from matplotlib import pyplot as plt

from cogef import COGEF1D, MPF
from cogef import Dissociation
from cogef.dissociation.diss_old import Dissociation as Dissociation_old
from cogef.units import nN#, _hplanck, _e
import numpy as np

#h = _hplanck / _e

cogef = COGEF1D(0, 2)
energies, distances = cogef.get_energy_curve()
E = list(energies)
last_intact_bond = np.argmax(E)

diss_old = Dissociation_old(cogef)
cogef.last_intact_bond_image = last_intact_bond

D0 = diss_old.electronic_energy_barrier(0)
print('De=', D0, 'eV')

Fmax_cogef = cogef.get_maximum_force(method='use_energies')  # eV/A
print('Fmax = {0:4.2f} eV/A'.format(Fmax_cogef), '=', Fmax_cogef / nN, 'nN')

mpf = MPF(D0, Fmax_cogef)
fstep = 0.0001

# values for calculation
T = 300
anN = 10  # nN/s
print(' ')
print('For T =', T, 'K and alpha =', anN, 'nN/s')

# ------------------ Relative forces (f = F/Fmax)-----------------------------
fmp_bell = mpf.bell(T_K=T, alpha_nNs=anN)

delf = mpf.width_f(T_K=T)
b1 = (fmp_bell + delf / 2)
b2 = (fmp_bell - delf / 2)

fmp_gen = mpf.general(T_K=T, alpha_nNs=anN)
g1 = (fmp_gen + delf / 2)
g2 = (fmp_gen - delf / 2)

fmp_gen_ver = mpf.general_verify(fmp_gen, T_K=T, alpha_nNs=anN)

#################################################
dpdf_bell, f_dpdf_bell = mpf.bell_pd(T_K=T, alpha_nNs=anN, force_step=fstep,
                                     prob_break_value=0.)
idx_bell = np.argmax(dpdf_bell)
fmp_bell_dpdf = f_dpdf_bell[idx_bell]

################################################
dpdf_gen, f_dpdf_gen = mpf.general_pd(T_K=T, alpha_nNs=anN, force_step=fstep,
                                      prob_break_value=0.)
idx_gen = np.argmax(dpdf_gen)
fmp_gen_dpdf = f_dpdf_gen[idx_gen]

print('        Actual most probable force  (F) values in nN          ')
fmp_bell = round(fmp_bell * Fmax_cogef / nN, 4)
b1 = round(b1 * Fmax_cogef / nN, 4)
b2 = round(b2 * Fmax_cogef / nN, 4)
print('Most probable force_Bell is', fmp_bell, 'nN ranging '
                                               'from', b2, 'to', b1, 'nN')
fmp_bell_dpdf = round(fmp_bell_dpdf * Fmax_cogef / nN, 4)
print('From numerical, maximum force_Bell is', fmp_bell_dpdf, 'nN')

print(' ')
fmp_gen = round(fmp_gen * Fmax_cogef / nN, 4)
g1 = round(g1 * Fmax_cogef / nN, 4)
g2 = round(g2 * Fmax_cogef / nN, 4)
print('Most probable force_General:', fmp_gen, 'nN ranging '
                                               'from', g2, 'nN to', g1, 'nN')
fmp_gen_ver = round(fmp_gen_ver * Fmax_cogef / nN, 4)
print('From verification, MPF_General:', fmp_gen_ver, 'nN')

fmp_gen_dpdf = round(fmp_gen_dpdf * Fmax_cogef / nN, 4)
print('From numerical, maximum force_General is', fmp_gen_dpdf, 'nN')

# numerical values in nN
f_dpdf_bell = f_dpdf_bell * Fmax_cogef / nN
f_dpdf_gen = f_dpdf_gen * Fmax_cogef / nN

fig = plt.figure()
ax = fig.add_subplot()
plt.plot(f_dpdf_bell, dpdf_bell, 'o-', color='orange', label='Bell')
plt.plot(f_dpdf_gen, dpdf_gen, 'o-', color='cornflowerblue', label='General')
plt.axvline(x=b1, ls='--', color='orange')
plt.axvline(x=b2, ls='--', color='orange')
plt.axvline(x=g1, ls='--', color='cornflowerblue')
plt.axvline(x=g2, ls='--', color='cornflowerblue')
plt.xlim(0.60, 1.15)
plt.xlabel('Force (nN)')
plt.ylabel('dp/dF (nN$^{-1}$)')
plt.title(r'$T$={} $K$,  '.format(T) + r'$\alpha$={} $nN/s$'.format(anN))
plt.legend()
plt.savefig('mpf.png')
