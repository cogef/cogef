from cogef import COGEF1D
from cogef.units import nN

cogef = COGEF1D(0, 2)  # this reads cogef1d_0_2/cogef.traj if it is there

print('Maximal force: {0:4.2f} nN (from energies),'.format(
    cogef.get_maximum_force(method='use_energies') / nN) +
    ' {0:4.2f} nN (from forces)'.format(
        cogef.get_maximum_force(method='use_forces') / nN))
