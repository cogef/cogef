 ### Rupture force calculation
 
  T = 300
  fmax = 0.01
  stepsize = 0.1
  max_steps = 100

  image = Atoms('AuAgAg', positions=((-1, 0, 0), (0, 0, 0), (1, 0, 0)))
  image.set_calculator(EMT())
  FIRE(image).run(fmax=fmax)

  cogef = COGEF([image], 0, 2, optimizer=FIRE, fmax=fmax)
  diss = Dissociation(cogef)
  for step in range(max_steps + 1):
      try:
          fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE,
                                             force_step=fstep,
                                             method='electronic')
          break
      except ValueError as msg:
          if diss.error not in [1, 2, 3]:
              raise ValueError(msg)
          assert step < max_steps, 'Step limit reached.'
          # Add one image
          cogef.pull(stepsize, 1, initialize, 'cogef.traj')
  force, error = diss.rupture_force_and_uncertainty(
      T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
  # in nN
  force = force / nN
  error = error / nN
  print('Rupture force: ({0:4.3f} +- {1:4.3f}) nN'.format(force, error))
