import numpy as np
from pathlib import Path

from ase import io
from ase.build import molecule
from ase.calculators.emt import EMT

from cogef.dcogef import DCOGEF


biphenyl = molecule('biphenyl')
dihedral = [1, 0, 14, 15]


def test_rotate_biphenyl():
    atoms = biphenyl.copy()
    atoms.calc = EMT()

    def initialize(atoms):
        atoms.calc = EMT()
        return atoms

    name = 'othername'
    dcg = DCOGEF([atoms], dihedral, name=name,
                 initialize=initialize, fmax=1e13)

    n = 3
    dcg.move(2 * np.pi / n, n)

    expected_name = name
    for index in dihedral:
        expected_name += f'_{index}'
    fname = Path(expected_name) / 'dcogef.traj'
    assert fname.is_file()
    traj = io.Trajectory(fname)
    assert len(traj) == n + 1

    for image in traj:
        assert len(image.constraints) == 1
        print(image.constraints)
