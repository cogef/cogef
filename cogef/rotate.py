import numpy as np
from typing import List, Tuple

from ase import Atoms
from ase.build.connected import connected_indices


def rotate_around_axis(axis, vector, angle: float):
    """Rotate vector around axis.

    Parameters
    ----------
    axis: numpy array of three floats
    vecor: numpy array of three floats
    angle: float

    Returns
    -------
    result: numpy array of three floats

    """
    phi, theta = get_angles(axis)
    vector = rotate(vector, -phi)
    vector = rotate(vector, 0., theta)
    vector = rotate(vector, angle)
    vector = rotate(vector, 0., -theta)
    return rotate(vector, phi)


def get_angles(axis):
    """Get phi and theta angles from the axis.

    Parameters
    ----------
    axis: numpy array of three floats

    Returns
    -------
    phi: float
    theta: float

    """
    x, y, _ = axis
    # Get phi
    d = np.sqrt(x**2 + y**2)
    if d == 0:
        phi = 0.
    else:
        if y >= 0:
            phi = np.arccos(x / d)
        else:
            phi = -np.arccos(x / d)
    # Now set axis in xz-plane, pointing towards positive x
    x, y, z = rotate(axis, -phi)
    # Get theta
    d = np.sqrt(x**2 + z**2)
    if d == 0:
        theta = 0.
    else:
        theta = np.arccos(z / d)
    return phi, theta


def rotate(vector, phi, theta=0., psi=0.):
    """Rotate vector by phi, theta and psi angles.

    Parameters
    ----------
    axis: numpy array of three floats
    phi: float
    theta: float
    psi: float

    Returns
    -------
    result: numpy array of three floats

    """
    sinphi = np.sin(phi)
    cosphi = np.cos(phi)
    sintheta = np.sin(theta)
    costheta = np.cos(theta)
    sinpsi = np.sin(psi)
    cospsi = np.cos(psi)
    rot_mat = np.array(
        [[cosphi * costheta, -sinphi * cospsi - cosphi * sintheta * sinpsi,
            sinphi * sinpsi - cosphi * sintheta * cospsi],
         [sinphi * costheta, cosphi * cospsi - sinphi * sintheta * sinpsi,
            -cosphi * sinpsi - sinphi * sintheta * cospsi],
         [sintheta, costheta * sinpsi, costheta * cospsi]
         ])
    return np.dot(rot_mat, vector)


def connected_to_1_if_0_removed(
        atoms: Atoms, i0: int, i1: int, **kwargs) -> List[int]:
    atoms_copy = atoms.copy()
    atoms_copy[i0].symbol = 'X'
    return connected_indices(atoms_copy, i1, **kwargs)


def splitted_indices(atoms: Atoms,
                     index1: int, index2: int,
                     **kwargs) -> Tuple[List[int], List[int]]:
    def connected(i1, i2):
        return connected_to_1_if_0_removed(atoms, i1, i2, **kwargs)

    return connected(index2, index1), connected(index1, index2)
