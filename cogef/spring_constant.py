from sys import float_info
from typing import List, Optional
import numpy as np


def quadratic(xlist: List[float], ylist: List[float],
              deg: int = 2) -> List[float]:
    """Quadratic fit to a one dimensional function

    Returns: (x0, y0, a2)
      x0: x-value of the minimum
      y0: y-value of the minimum
      a2: second derivative at the minimum
    """
    assert len(xlist) == len(ylist)
    pars = np.polyfit(xlist, ylist, deg)
    a2, a1, a0 = pars[-3:]
    x0 = - a1 / 2 / a2
    y0 = a0 + a1 * x0 + a2 * x0**2

    return x0, y0, a2


def spring_constant(distances: List[float], energies: List[float],
                    dmin: float = -float_info.max,
                    dmax: float = float_info.max,
                    first: int = 0, last: Optional[int] = None) -> List[float]:
    """Obtain the spring constant

    dmin: minimal distance to consider
    dmax: maximal distance to consider
    first: starting index to consider
    last: endig index to consider

    Returns: (x0, y0, k)
      x0: x-value of the minimum
      y0: y-value of the minimum
      k: spring constant
    """
    x = distances[first:last]
    y = energies[first:last]
    select = np.where((x >= dmin) & (x <= dmax))
    d0, e0, a2 = quadratic(x[select], y[select])

    return d0, e0, 2 * a2
